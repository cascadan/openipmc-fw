# OpenIPMC-FW

OpenIPMC-FW is a reference firmware designed for OpenIPMC-HW mini-DIMM card, and was developed on STM32CubeIDE.

OpenIPMC-FW includes a port of OpenIPMC to perform IPMI messaging for ATCA boards.

## Documentation

OpenIPMC-FW is a reference firmware and need to be adapted for the target ATCA board. For more details and developments instructions, please find the documentation pages at [openipmc.gitlab.io/openipmc-fw/](https://openipmc.gitlab.io/openipmc-fw/)

## Licensing

OpenIPMC-FW is Copyright 2020-2021 of Andre Cascadan, Luigi Calligaris. OpenIPMC-FW is released under GNU General Public License version 3. Please refer to the LICENSE document included in this repository.
