#ifndef ST_BOOTLOADER_H
#define ST_BOOTLOADER_H


void st_bootloader_launch(void);

void st_bootloader_jump_if_scheduled(void);


#endif //ST_BOOTLOADER_H
